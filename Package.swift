// swift-tools-version:4.0
import PackageDescription

let packages = Package(
    name: "CPangoCairo",
    pkgConfig: "pangocairo",
    products: [
      .library(
        name: "CPangoCairo",
        targets: ["CPangoCairo"]
      )
    ],
    targets: [
      .target(
        name: "CPangoCairo",
        path: ".")
    ]
)
